package nl.utwente.di.bookQuote;

import java.util.HashMap;
import java.util.Map;

public class Quoter {
    private Map<String,Double> prices = new HashMap<>();
    public double getBookPrice (String isbn) {
        prices.put(String.valueOf(1), 10.0);
        prices.put(String.valueOf(2), 45.0);
        prices.put(String.valueOf(3), 30.0);
        prices.put(String.valueOf(4), 25.0);
        prices.put(String.valueOf(5), 50.0);

        return prices.getOrDefault(String.valueOf(isbn), 0.0);
    }
}
